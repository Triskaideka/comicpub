# Poor Man's Comic Publisher

The Poor Man's Comic Publisher (PMCP) is a PHP script for publishing webcomics.  It is intended to be easy for the user to install and to understand.  Basically, you put a few files on your webserver, do a little customization (optional), then give  your comic images a certain filename format and upload them.


## Features

- Allows comics to be uploaded before their publication date.
- Supports news posts associated with particular comics.
- Includes an archive page that lists every comic to date.
- Automatically provides a syndication feed (using the Atom standard).
- Optional keyboard navigation (via left &amp; right arrow keys).
- Link to view a random comic.
- The way the page looks is almost completely customizable (if you know HTML &amp; CSS).
- The whole thing runs off a single, less-than-500-lines PHP script (with other optional files for customization).


## Limitations

PMCP might **not** be the right tool for you if:

- you don't have access to a web server where you can upload files and run PHP scripts.
- you already have comics in another system that you need to import -- there is no automated support for this, and you may find it time-consuming to do manually.
- your comics sometimes comprise more than one image file.
- you ever need to publish more than one comic in a day.
- you can't draw -- PMCP can't help you with the creative part at all.


## Bare Minimum Instructions

If you want to set up PMCP with the absolute least amount of work:

- Put index.php on your web server, in a directory where you can run PHP scripts.  Make sure it has whatever permissions it needs to be executed by the web server.
- In the same directory, upload your comics, as image files, with names in the format `yyyy-mm-dd-name.ext`.
  - `yyyy-mm-dd` is the date on which the comic was, or should be, published.
  - The `name` part is optional, but recommended to help you remember which comic is which.  It will also help prevent your readers from seeing comics before they're supposed to be published, by making the filenames harder to guess.
  - `ext` can be any of `png`, `gif`, `jpg`, or `jpeg`.
- Open a web browser and visit the address of the index.php script.  If you see your comic and don't see any error messages, then you're set up!

Thereafter, each time you want to publish a new comic, just upload its image file to the same directory, named in the format described above.

To publish a news post along with any comic, upload it as a text or HTML file.  Its name should be identical to the filename of the comic that it accompanies, except with the extension part replaced with either "txt" or "html", as appropriate.


## Optional Configuration

Once everything's working, you might want to do some customization.  Customization is done mainly by editing one of two files: the settings file (pmcp.ini) and the template for the web page (template.html).

This section tells you what settings you can change, and how.  If it seems long, you can safely ignore it until you want to change something.


### Settings File

Copy the pmcp.ini file to the same directory as index.php, if it's not there already, then edit it to change any of the following settings.

Lines in INI files that begin with a semicolon (`;`) are comments for the benefit of the user (that's you), and are ignored by the script.  Lines that establish settings are in the form "setting = value".  Note that the characters `?{}|&~![()^"` have special meaning in INI files.  If you need to use one of them and it doesn't display correctly, try enclosing the entire value in double quotes, e.g.:

> ```author_name = "Hansel & Gretel"```

#### series_title

The title of the whole comic (as opposed to just one strip).  It'll appear in appropriate places on each comic's page, and in the Atom feed.

#### author_name

The name of the comic's author/artist.  It appears only in the Atom feed.

#### author_email

The e-mail address of the comic's author/artist.  It appears only in the Atom feed.

#### comic_dir

Sets the directory where the comics are stored.  It can be relative to the directory where index.php is, or fully qualified (starts with a slash).  The default is the same directory as index.php.

#### news_dir

Sets the directory where the news posts are stored.  It can be the same as `comic_dir`, but it doesn't have to be.  It also defaults to the same directory as index.php.

#### auxiliary_files_dir

Sets the directory where template.html and the navigation icons are stored.  As before, it may or may not be the same as `comic_dir` and/or `news_dir`, and the default value is the same directory as index.php is in.  Note that even if template.html is stored in a subdirectory, if it uses relative links, they should be relative to index.php's directory.  (Also, if you're running PMCP on a Windows machine, don't try to name this directory "aux", because Windows forbids naming folders "aux" for some reason.  I found that one out the hard way.)

#### template_url

If your template HTML file is hosted on another server, or just needs to be passed through your web server before it can be used, then set `template_url` to the file's full URL.  If you leave this commented out, or if PMCP can't find any file at this URL, it will look for a file named "template.html" in the auxiliary_files_dir instead.

#### time_zone

PMCP will use the UTC time zone unless you set `time_zone` to one of the time zones listed at http://php.net/manual/en/timezones.php .

#### detect_comic_titles

If `detect_comic_titles` is set to 1, PMCP will look at the first line of any HTML news post to see if it contains a header tag (e.g. `<h2>` ... `</h2>`).  If there is a header, it will be used as the title for the strip.  Otherwise, generic text containing the series title and the date will be used.

#### num_comics_in_feed

Sets how many comics should be included in the Atom feed.  The default is 10.  You don't need to keep comics in the feed forever, but a good rule of thumb is to include enough so that people won't miss any if they (and their feed readers) go on vacation for a few weeks.

#### feed_news_limit

Sets the (approximate) maximum length (in characters) of the news shown in each entry in the Atom feed.  Use something large enough that most of your news posts won't be truncated; the point is not to cut legitimate news short, but to prevent unmanageably large feeds in exceptional cases.  The default is 10000.

#### archive_reverse

By default, the archive will be shown in chronological order (oldest at top).  Set `archive_reverse` to `1` to show it in reverse chronological order (newest at top).

#### click_comic_to_advance

If `click_comic_to_advance` is set to `1`, the comic image itself will function as a duplicate "next" link.  Set to `0` to disable.

#### link_prefix

Warning: using this value at all, even just setting it to an empty string, *will break all your links* unless you also configure your web server to handle the new value.

By default, PCMP generates links to other PMCP pages using the format `?show=[something]`, where [something] is either the date of a particular strip, or a word like "archive" or "feed".  So, if your comic lives at `http://example.com/comic/`, then the strip from December 31st, 2023 would be at `http://example.com/comic/?show=2023-12-31`.  But maybe you think that URL isn't pretty enough, and you want it to be something like `http://example.com/comic/2023-12-31` instead.

To effect this change, you must do two things.  First, set up URL rewriting on your web server so that it interprets the address you want to see in the browser as if it were in PMCP's native format.  Second, change the value of `link_prefix` to whatever should come before the date/keyword in the links that PMCP generates.

The way to set up URL rewriting will be different depending on which webserver you're using, and maybe also on other factors such as your operating system, how the webserver is configured, etc.  So you may need to consult your webserver's documentation and/or ask your system administrator.  But here's one example.  Let's say your server is Apache and you have [mod_rewrite](http://httpd.apache.org/docs/current/mod/mod_rewrite.html) enabled.  In pmcp.ini, you could set:

> `link_prefix = ""`

And then in an `.htaccess` file in the same directory, you could include these lines:

> `RewriteEngine On`<br>
`RewriteRule (?!\?)([0-9]{4}-[0-9]{2}-[0-9]{2})$ ?show=$1`

Now you can use `http://example.com/comic/2023-12-31` to link to your comics, _and_ PMCP's own links will look like that too.

The default value for link_prefix is `"?show="`, and it should always be safe to use that value even if your web server has been configured to handle other possibilities.


### Customize the Web Page

If you know how to write HTML, you can customize what the web page looks like.  Copy template.html to the same directory as index.php, and modify it.  (You can also host template.html on another server; see the `template_url` setting in pmcp.ini.)  Mark where the comic, title, etc. should appear on the page by using codes within HTML comments.  The recognized codes are:

#### &lt;!-- PMCP:AUTODISCOVERY --&gt;

Insert an HTML tag that will help web browsers find the syndication feed automatically.  This code should be inside the page's `<head>` element.

#### &lt;!-- PMCP:COMIC --&gt;

On the comic page, insert the comic image wrapped inside a `<div id="comic">`.  On the archive page, insert the list of comics.

#### &lt;!-- PMCP:CONTENT --&gt;

This is shorthand for `<!--PMCP:COMIC-->` followed by `<!--PMCP:CONTROLS-->` followed by `<!--PMCP:NEWS-->`.

#### &lt;!-- PMCP:CONTROLS --&gt;

Insert the links that let the reader use & navigate PMCP's features (previous, next, archive, etc., but note that "back" and "forward" are *not* included).  This set of links is wrapped in a `<div id="controls">`.  If you want to rearrange the links or change which ones are displayed, use the individual `<!--PMCP:LINK_*-->` codes instead of this code.

#### &lt;!-- PMCP:DATE --&gt;

Insert the date of the comic being displayed, formatted like "31 December 2023".  Ignored on the archive page.

#### &lt;!-- PMCP:KEYNAV --&gt;

Insert JavaScript code that will allow users to navigate between your comics by pressing the left arrow and right arrow keys.  This should work regardless of whether you put it in your document's `<head>` or `<body>`.

#### &lt;!-- PMCP:LINK_* --&gt;

There are separate codes for each of the links that PMCP generates.  They are `LINK_FIRST`, `LINK_BACK`, `LINK_PREVIOUS`, `LINK_ARCHIVE`, `LINK_RANDOM`, `LINK_SUBSCRIBE`, `LINK_NEXT`, `LINK_FORWARD`, and `LINK_NEW`.  If you don't want to bother with managing all of these codes individually, consider using `<!--PMCP:CONTROLS-->` as a shortcut.

I hope most of those names are self-explanatory.  The difference between "BACK" and "PREVIOUS" is that the "previous" link always goes to the immediately preceding comic, while "back" takes you back several comics.  Likewise, "NEXT" is always the immediately following comic, while "FORWARD" jumps ahead by several comics.  The exact number is determined by the "jump_by" setting in pmcp.ini (or it defaults to 7).

#### &lt;!-- PMCP:NEWS --&gt;

Insert the news post, wrapped inside a `<div id="news">`.

#### &lt;!-- PMCP:TITLE --&gt;

Insert the title of the comic being displayed.  Read about detect_comic_titles in pmcp.ini for information about how the title is determined.  If this setting is disabled or a title cannot be found, the series title and comic date will be used.  When viewing the archive, the word "Archive" is used as the title.


### Renaming Files & Directories

index.php can be renamed with no adverse effects.  However, pmcp.ini (if it is used) cannot be renamed.  It must be in the same directory as index.php.

The comics and newsposts may reside in the same directory as index.php, or may reside in the `comic_dir` directory named in pmcp.ini.

The template.html file cannot be renamed and must be in the `auxiliary_files_dir` named in pmcp.ini.

The navigation images (if used) must be in the `auxiliary_files_dir`, or if there is none, then in the same directory as index.php.  Their names must be:

- archive.png
- back.png
- back-off.png
- first-off.png
- first.png
- forward.png
- forward-off.png
- latest-off.png
- latest.png
- next-off.png
- next.png
- previous-off.png
- previous.png
- random.png
- subscribe.png

You can use the default images in the PMCP repository, or create your own in PNG, GIF, or JPEG format.  The images with `-off` in their names are for when those links are "turned off" when viewing either the first or the latest comic.

If these images aren't present, PMCP will use text links instead.  To rearrange or suppress these links, see the instructions on using `PMCP:LINK_*` codes in the template.


## License

Since version 2.0.0, the Poor Man's Comic Publisher is available under the terms of the MIT license (see LICENSE.txt).  In short, this means you can do pretty much whatever you want with it, but if you redistribute the code or a modified version of the code, you have to keep the copyright notice and license intact.

Versions prior to 2.0.0 were in the public domain and may still be used accordingly.


## Changelog

Version 3.0.0 dropped support for Textile markup.  Unfortunately, Textile lost the format war to Markdown, and supporting it has been increasingly awkward.  If you're upgrading from a 1.x or 2.x version, you should manually convert news posts to either HTML or plain text.  Version 3 also rewrites some now-deprecated PHP code, which is probably the main reason you would want to upgrade.

Version 2.3.0 added the "back" and "forward" links, which are like the previous and next links except that they jump backwards or forwards by several comics at a time.

Version 2.2.1 lifted the restriction that the comic images' file extensions had to be in lowercase.  (The navigation images' filenames still have to be in all lowercase.)

Version 2.2.0 added the `link_prefix` option to give you more control over your URLs.

Version 2.1.0 consolidated documentation in this README file, and updated the keyboard navigation code so that it's safe to use arrow keys inside `<TEXTAREA>` and `<INPUT>` elements.

Version 2.0.0 changed the license to MIT, added the "random comic" button, and added some new `<!--PMCP:-->` tags to allow greater flexibility in designing the template page.  Aside from the new license, it should be fully backwards compatible with 1.x.x versions, meaning that you should not have to change any files other than index.php if you want to upgrade.
