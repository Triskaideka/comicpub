<?php /*
Poor Man's Comic Publisher
Version 3.0.0
License: MIT (see LICENSE.txt)
https://triskaideka.net/comicpub/
https://gitlab.com/Triskaideka/comicpub
See README.md for documentation.
*/

### Variables
if (file_exists('pmcp.ini'))  {
  $ini = parse_ini_file('pmcp.ini', false);
}

# $link_prefix is allowed to be an empty string, so this test must account for that possibility
if ( array_key_exists('link_prefix', $ini) )  {
  $link_prefix = $ini['link_prefix'];
}  else  {
  $link_prefix = '?show=';
}

if ( array_key_exists('jump_by', $ini) )  {
  $some = $ini['jump_by'];
}  else  {
  $some = 7;
}

if (!isset($ini['time_zone']) || !date_default_timezone_set($ini['time_zone']))  {
  date_default_timezone_set('UTC');
}

if (isset($_GET["show"]))  { $requested_date = $_GET["show"]; }
else  { $requested_date = date("Y-m-d"); }

# Set auxiliary files directory, without a trailing slash
$aux_dir = '.';
if ( isset($ini['auxiliary_files_dir']) && $ini['auxiliary_files_dir'] )  {
  $aux_dir = preg_replace("/\/$/", '', $ini['auxiliary_files_dir']);
}

# $base_addr is the URL of the web page being viewed
$base_addr = strtolower(preg_replace("/\/.*$/", "", $_SERVER["SERVER_PROTOCOL"])) . "://" . $_SERVER["HTTP_HOST"] . preg_replace("/\?.*$/", "", $_SERVER["REQUEST_URI"]);

### Class for comic objects
class comic {

  # Normally I think it's recommended not to make class variables public, but since this class exists almost solely for its
  # variables, I'm making an exception.  Plus, I hear that "naive getters and setters" can slow execution time.
  public $filename = '';
  public $path = '';
  public $date = 0;
  public $year = 0;
  public $month = 0;
  public $day = 0;
  #public $time = 0;   # see below
  public $feed_time = '';

  # But we use get_* functions for these, mostly to save memory by not loading them if we don't need them.
  protected $news = '';
  protected $title = '';

  public function __construct($fname)  {
    preg_match("/(([0-9]{4})-([0-9]{2})-([0-9]{2})).*\.(png|gif|jpe?g)$/i", $fname, $matches);
    $this->path = $fname;
    $this->filename = $matches[0];
    $this->date = $matches[1];  # contains the comic's date of publication in year-month-day format
    $this->year = $matches[2];
    $this->month = $matches[3];
    $this->day = $matches[4];
    #$this->time = mktime(0, 0, 0, $this->month, $this->day, $this->year);   # avoid using this because of the year 2038 problem
    $this->feed_time = date(DATE_ATOM, mktime(0, 0, 0, $this->month, $this->day, $this->year));
  }

  public function get_title()  {
    # Load & return the title for the individual strip, in HTML format.

    if (empty($this->title))  {
      global $ini;

      if (empty($this->news))  {
        $this->get_news();  # loads $this->news
      }

      # If the setting to detect comic titles is on, we look for a <h1>, <h2>,
      # etc. in the news post.
      if (   (isset($ini['detect_comic_titles']) && $ini['detect_comic_titles'])
          && preg_match("/^\s*<h[1-6]>(.*)<\/h[1-6]>/U", $this->news, $matches)
         )
      {

        $this->title = $matches[1];

      }  else  {  # We can't or don't want to get the title from the news post.

        $this->title = (isset($ini["series_title"]) ? "$ini[series_title] &mdash; c" : "C");  # continued on next line
        $this->title .= "omic for " . date("l, j F Y", mktime(0, 0, 0, $this->month, $this->day, $this->year));

      }
    }

    return $this->title;
  }

  public function get_news()  {
    # Load & return the news post.  We make this part of the object because it's displayed both on the comic
    # page and in the feed, but we separate it from the constructor so we don't load news that we don't need.
    # The news post is returned in HTML format.

    if (empty($this->news))  {
      global $ini;

      # Figure out whether a news post exists, and load it if so.
      $newsfile = ( isset($ini['news_dir']) ? $ini['news_dir'] : '.' ) . '/' . preg_replace("/(png|gif|jpe?g)$/i", "html", $this->filename);  # .html if it exists
      if (!file_exists($newsfile))  { $newsfile = preg_replace("/html$/", "txt", $newsfile); }  # if not, try .txt
      if (file_exists($newsfile))  {
        $this->news = file_get_contents($newsfile);
      }

      # Treat a .txt file as plain text.
      if (preg_match("/txt$/", $newsfile)) {
        $this->news = htmlspecialchars($this->news);
      }
    }

    return $this->news;
  }

}  # end class comic


### Sorting function
function comcmp($a, $b)  {
# Used to usort() the comic list.
# Returns 0 if both comics are on the same day, -1 if the first comic is earlier than the second, and 1 if the second is earlier than the first.
  if ($a->year != $b->year)  {
    return ($a->year < $b->year) ? -1 : 1;
  }  else  {
    if ($a->month != $b->month)  {
      return ($a->month < $b->month) ? -1 : 1;
    }  else  {
      if ($a->day != $b->day)  {
        return ($a->day < $b->day) ? -1 : 1;
      }  else  {
        return 0;
      }
    }
  }
}   # end function comcmp()


### Function to replace <!-- PMCP: --> codes with the appropriate content.
  # Currently recognized codes are CONTENT, TITLE, AUTODISCOVERY, KEYNAV,
  # and DATE.
  # This is basically a wrapper for preg_replace that knows a little about
  # the pattern we want to look for.
function pmcp_insert($code, $replacement, $page)  {
  return preg_replace("/<!--\s*PMCP\s*:\s*$code\s*-->/i", $replacement, $page);
}   # end function pmcp_insert()


### Utility function to remove leading whitespace in heredocs.  Makes the output a little cleaner & lighter.
  # http://stackoverflow.com/questions/1655159/php-how-to-trim-each-line-in-a-heredoc-long-string
function trim_heredoc($t)  {
  return implode("\n", array_map('trim', explode("\n", $t)));
}


### Load template
$template = '';

# Use the template_url setting if it exists.
if (isset($ini['template_url']))  {
  $template = file_get_contents($ini['template_url']);
}

# If there's no template_url, or we didn't get anything from it, look for template.html in the auxiliary_files_dir.
if ( empty($template) && file_exists("$aux_dir/template.html") )  {
  $template = file_get_contents("$aux_dir/template.html");
}

# If there's no template_url and no template.html, use a default template.
if (empty($template))  {
$template = <<<STOP
<!doctype html>
<html>
<head>
<!-- PMCP:AUTODISCOVERY -->
<title><!-- PMCP:TITLE --></title>
</head>
<body>
<h1><!-- PMCP:TITLE --></h1>
<div><!-- PMCP:CONTENT --></div>
</body>
</html>
STOP;
}


### Create comic list
$comic_dir = '.';
if (isset($ini['comic_dir']) && is_dir($ini['comic_dir']))  {
  $comic_dir = preg_replace("/\/$/", '', $ini['comic_dir']);  # trailing slash not allowed; we'll add it as necessary
}
$today_dummy = new comic(date('Y-m-d') . '.png');   # just so we have today's date to comcmp() each file with

# Read files from the directory, add them to the list if their names are in the right format and their dates are today or earlier
$dirhandle = opendir($comic_dir) or die("Couldn't open the directory \"$comic_dir\".");
while (false !== ($cfile = readdir($dirhandle)))  {
  if (!is_file("$comic_dir/$cfile") || !preg_match("/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}.*\.(png|gif|jpe?g)$/i", $cfile))  { continue; }
  $c = new comic("$comic_dir/$cfile");
  if (comcmp($c, $today_dummy) < 1)  {
    $comiclist[] = $c;
  }
}
closedir($dirhandle);

if (empty($comiclist))  {
  echo "No comics were found in the \"$comic_dir\" directory.  If there are images there, make sure their names are in the correct format.";
  exit;
}

usort($comiclist, "comcmp");  # not sure readdir() is guaranteed to return the files in any particular order, so let's sort them

# Figure out which comic we're supposed to be displaying
$displayindex = count($comiclist) - 1;  # default to the last one if we don't find another one
for ($i = count($comiclist) - 1; $i >= 0; $i--)  {
  if (strcmp($comiclist[$i]->date, $requested_date) == 0)  {
    $displayindex = $i;
    break;
  }
}


# The random comic function will break if anything is echoed before the start of the following conditional.

### Write Atom feed
if (strcmp($requested_date, "feed") == 0)  {

  $num_comics_in_feed = 10;  # default
  if (isset($ini['num_comics_in_feed']) && $ini['num_comics_in_feed'] > 0)  {
    $num_comics_in_feed = $ini['num_comics_in_feed'];
  }

  header('Content-type: application/atom+xml');  # it's important that we don't echo anything before this line

  $content  = "";
  $content .= "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";

  $content .= "<feed xmlns=\"http://www.w3.org/2005/Atom\">\n";
  $content .= "  <id>$base_addr{$link_prefix}feed</id>\n";
  $content .= "  <title type=\"html\">" . (isset($ini["series_title"]) ? htmlspecialchars($ini["series_title"], ENT_NOQUOTES, 'UTF-8') : "A Webcomic") . "</title>\n";

  # In the next line we can't use an existing comic's feed_time because we're setting the date to one second after the
  # last existing comic's feed date.  We're doing this because <updated> fields have to be unique within the feed.
  $content .= "  <updated>" . date(DATE_ATOM, mktime(0, 0, 1, $comiclist[count($comiclist)-1]->month, $comiclist[count($comiclist)-1]->day, $comiclist[count($comiclist)-1]->year)) . "</updated>\n";

  $content .= "  <link rel=\"self\" href=\"$base_addr{$link_prefix}feed\" type=\"application/atom+xml\" />\n";

  $content .= "  <author>\n";
  $content .= "    <name>" . ( isset($ini['author_name']) ? $ini[author_name] : 'Anonymous' ) . "</name>\n";
  $content .= "    <uri>$base_addr</uri>\n";
  if (isset($ini['author_email'])) { $content .= "    <email>$ini[author_email]</email>\n"; }
  $content .= "  </author>\n";

  # Loop through the last N comics
  for ($i = count($comiclist) - 1; $i >= 0 && $i >= count($comiclist) - $num_comics_in_feed; $i--)  {
    $content .= "  <entry>\n";
    $content .= "    <title type=\"html\">" . htmlspecialchars( $comiclist[$i]->get_title(), ENT_NOQUOTES, 'UTF-8' ) . "</title>\n";
    $content .= "    <id>$base_addr$link_prefix" . $comiclist[$i]->date . "</id>\n";
    $content .= "    <published>" . $comiclist[$i]->feed_time . "</published>\n";
    $content .= "    <updated>" . $comiclist[$i]->feed_time . "</updated>\n";
    $content .= "    <link href=\"$base_addr$link_prefix" . $comiclist[$i]->date . "\"/>\n";
    $content .= "    <summary>Comic for " . date("Y F j", mktime(0, 0, 0, $comiclist[$i]->month, $comiclist[$i]->day, $comiclist[$i]->year)) . "</summary>\n";

    $fnlimit = (isset($ini['feed_news_limit']) && $ini['feed_news_limit'] > 0 ? $ini['feed_news_limit'] : 10000);
    $feedcontent = strip_tags($comiclist[$i]->get_news());
    if (strlen($feedcontent) > $fnlimit)  {  # truncate news if need be:
      $feedcontent = substr_replace($feedcontent, '', $fnlimit);  # truncate to limit
      $feedcontent = preg_replace('/\S+$/', '', $feedcontent);  # remove any word (i.e. non-space characters) at the end
      $feedcontent .= "...";  # then insert an ellipsis
    }
    $feedcontent = htmlspecialchars("<div><img src=\"" . $comiclist[$i]->path . "\" alt=\"Comic image\" /></div>" . $feedcontent);
    $content .= "    <content type=\"html\">$feedcontent</content>\n";

    $content .= "  </entry>\n";
  }

  $content .= "</feed>\n";


### Or, redirect to random comic
}  elseif (strcmp($requested_date, 'random') == 0)  {

  $relative_uri = preg_replace("/random$/",
                               $comiclist[array_rand($comiclist)]->date,  # select the identifier of a random comic
                               $_SERVER['REQUEST_URI']);

  # http://httpstatus.es/307
  # http://stackoverflow.com/questions/2068418/whats-the-difference-between-a-302-and-a-307-redirect
  header("HTTP/1.1 307 Internal Redirect", true, 307);

  # http://tools.ietf.org/html/rfc7231#section-7.1.2
  header("Location: $relative_uri");


### Or, write archive
}  elseif (strcmp($requested_date, 'archive') == 0)  {

  # Show archive in reverse chronological order?
  if (isset($ini['archive_reverse']) && $ini['archive_reverse'])  {
    $comiclist = array_reverse($comiclist);  # The comic list will remain reversed for the rest of the script, but that should be okay.
  }

  # Assemble the archive code
  $alist = "<section id=\"archive\">\n<h2>" . $comiclist[0]->year . "</h2>\n<ul>\n";
  for ($i = 0; $i < count($comiclist); $i++)  {
    if ($i > 0 && $comiclist[$i-1]->year != $comiclist[$i]->year)  {  # header for new year
      $alist .= "</ul>\n\n<h2>" . $comiclist[$i]->year . "</h2>\n<ul>\n";
    }
    $alist .= "  <li><a href=\"$link_prefix"
            . $comiclist[$i]->date . "\">"
            . date("l, F j", mktime(0, 0, 0, $comiclist[$i]->month, $comiclist[$i]->day, $comiclist[$i]->year))
            . "</a></li>\n";
  }
  $alist .= "</ul>\n</section>\n";

  # Insert the archive code into the content
  $content = pmcp_insert('AUTODISCOVERY', "<link rel=\"alternate\" type=\"application/atom+xml\" href=\"$base_addr{$link_prefix}feed\" title=\"$ini[series_title]\">", $template);
  $content = pmcp_insert('TITLE', 'Archive', $content);
  # On a comic page, PMCP:CONTENT expands to COMIC + CONTROLS + NEWS, but there are no controls or news on the archive page.
  $content = pmcp_insert('CONTENT', $alist, $content);
  $content = pmcp_insert('COMIC', $alist, $content);


### Or, write comic page
}  else  {
  # The comic image
  $comiccode = '';
  $comiccode .= "\n<div id=\"comic\">\n";
  if ($ini['click_comic_to_advance'] && $displayindex < count($comiclist) - 1)  { $comiccode .= "<a href=\"$link_prefix" . $comiclist[$displayindex+1]->date . '">'; }
  $comiccode .= "<img src=\"" . $comiclist[$displayindex]->path . "\" alt=\"" . $comiclist[$displayindex]->get_title() . "\">";
  if ($ini['click_comic_to_advance'] && $displayindex < count($comiclist) - 1)  { $comiccode .= '</a>'; }
  $comiccode .= "\n</div><!-- id=\"comic\" -->\n";

  ## Controls (prev, next, etc.)
  # Default controls as text
  $controls = array(
    'first'     => '|&lt; First',
    'back'      => '&lt;&lt; Back',
    'previous'  => '&lt; Previous',
    'archive'   => 'Archive',
    'random'    => 'Random',
    'subscribe' => 'Subscribe (Atom)',
    'next'      => 'Next &gt;',
    'forward'   => 'Forward &gt;&gt;',
    'latest'    => 'Latest &gt;|'
  );

  # Images for controls, if they exist
  foreach (array_keys($controls) as $button)  {
    foreach (array('gif', 'jpg', 'jpeg', 'png') as $ftype)  {
      if (file_exists("$aux_dir/$button.$ftype"))  {
        $controls[$button] = "<img src=\"$aux_dir/$button";
        if (  # Check to see whether a "disabled" version of the image should be used
             (
                  ( $displayindex === 0 && ( strcmp($button, 'first') === 0 || strcmp($button, 'previous') === 0 ) )
               || ( $displayindex - $some < 0 && strcmp($button, 'back') === 0 )
               || ( $displayindex + $some >= count($comiclist) && strcmp($button, 'forward') === 0 )
               || ( $displayindex === count($comiclist) - 1 && ( strcmp($button, 'next') === 0 || strcmp($button, 'latest') === 0 ) )
             )
             && ( file_exists("$aux_dir/$button-off.$ftype") )
           )
        {
          $controls[$button] .= '-off';
        }
        $controls[$button] .= ".$ftype\" alt=\"" . ucfirst($button) . '">';
      }
    }
  }

  # Code for controls
  $links = array(
    'first' => ($displayindex > 0 ? '<a href="' . $link_prefix . $comiclist[0]->date . '">' : '<span>')
               . $controls['first']
               . ($displayindex > 0 ? '</a>' : '</span>'),

    'back' => ($displayindex >= $some
                  ? '<a href="' . $link_prefix . $comiclist[$displayindex-$some]->date . '" id="pmcp_back">'
                  : '<span>')
              . $controls['back']
              . ($displayindex >= $some ? '</a>' : '</span>'),

    'previous' => ($displayindex > 0 ? '<a href="' . $link_prefix . $comiclist[$displayindex-1]->date . '" id="pmcp_prev" rel="prev">' : '<span>')
                  . $controls['previous']
                  . ($displayindex > 0 ? '</a>' : '</span>'),

    'archive'   => "<a href=\"{$link_prefix}archive\">$controls[archive]</a>",
    'random'    => "<a href=\"{$link_prefix}random\">$controls[random]</a>",
    'subscribe' => "<a href=\"{$link_prefix}feed\">$controls[subscribe]</a>",

    'next' => ($displayindex < count($comiclist) - 1 ? '<a href="' . $link_prefix . $comiclist[$displayindex+1]->date . '" id="pmcp_next" rel="next">' : '<span>')
              . $controls['next']
              . ($displayindex < count($comiclist) - 1 ? '</a>' : '</span>'),

    'forward' => ($displayindex < count($comiclist) - $some
                       ? '<a href="' . $link_prefix . $comiclist[$displayindex+$some]->date . '" id="pmcp_forward">'
                       : '<span>')
                   . $controls['forward']
                   . ($displayindex < count($comiclist) - $some ? '</a>' : '</span>'),

    'new' => ($displayindex < count($comiclist) - 1 ? '<a href="' . $link_prefix . $comiclist[count($comiclist)-1]->date . '">' : '<span>')
             . $controls['latest']
             . ($displayindex < count($comiclist) - 1 ? '</a>' : '</span>')
  );

  # Note that LINK_BACK and LINK_FORWARD are intentionally missing from this list.
  $controlscode = trim_heredoc(<<<__STOP
    <div id="controls">
      <!--PMCP:LINK_FIRST-->
      <!--PMCP:LINK_PREVIOUS-->
      <!--PMCP:LINK_ARCHIVE-->
      <!--PMCP:LINK_RANDOM-->
      <!--PMCP:LINK_SUBSCRIBE-->
      <!--PMCP:LINK_NEXT-->
      <!--PMCP:LINK_NEW-->
    </div><!-- id="controls" -->
__STOP
  );

  # News post, if any
  if (strcmp($comiclist[$displayindex]->get_news(), '') != 0)  {
    $newscode = "\n<div id=\"news\">\n" . $comiclist[$displayindex]->get_news() . "\n</div><!-- id=\"news\" -->\n";
  }  else  {
    $newscode = '';
  }

  # Substitute all this into $content
  $content = pmcp_insert('AUTODISCOVERY', "<link rel=\"alternate\" type=\"application/atom+xml\" href=\"$base_addr{$link_prefix}feed\" title=\"$ini[series_title]\">", $template);
  $content = pmcp_insert('DATE', date("j F Y", mktime(0, 0, 0, $comiclist[$displayindex]->month, $comiclist[$displayindex]->day, $comiclist[$displayindex]->year)), $content);
  $content = pmcp_insert('TITLE', $comiclist[$displayindex]->get_title(), $content);

  # PMCP:CONTENT is a macro for COMIC + CONTROLS + NEWS.  Note that CONTROLS itself is a macro.
  $content = pmcp_insert('CONTENT', "<!--PMCP:COMIC-->\n<!--PMCP:CONTROLS-->\n<!--PMCP:NEWS-->", $content);
  $content = pmcp_insert('COMIC', $comiccode, $content);
  $content = pmcp_insert('CONTROLS', $controlscode, $content);
  $content = pmcp_insert('NEWS', $newscode, $content);

  # PMCP:LINK_* codes can be handled with a foreach loop
  foreach (array_keys($links) as $l)  {
    $content = pmcp_insert("LINK_$l", $links[$l], $content);
  }

  $content = pmcp_insert('KEYNAV', trim_heredoc(<<<____STOP
    <script type="text/javascript"><!--
      document.onkeyup = function(e) {
        if ( !(document.activeElement.tagName.toUpperCase() == 'TEXTAREA'
               || document.activeElement.tagName.toUpperCase() == 'INPUT') )
        {
          e = e || window.event;
          if (e.altKey || e.ctrlKey || e.shiftKey) { return; }
          var keycode = e.keyCode || e.which;
          if (keycode == 37 && document.getElementById('pmcp_prev')) {
            window.location = document.getElementById('pmcp_prev').href;
          } else if (keycode == 39 && document.getElementById('pmcp_next')) {
            window.location = document.getElementById('pmcp_next').href;
          }
          return false;
        }
      };
    // --></script>
____STOP
  ), $content);

}

### Display
echo $content;
